const express = require('express');
const router = express.Router();

// declare axios for making http requests
const axios = require('axios');
const API = 'https://jsonplaceholder.typicode.com';

var DivJuris = require("../models/div-juris").DivJuris;

/*
router.get('/', (req, res) => {
  res.send('api works');
});

// Get all posts
router.get('/posts', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB
  axios.get(`${API}/posts`)
    .then(posts => {
      res.status(200).json(posts.data);
    })
    .catch(error => {
      res.status(500).send(error)
    });
});
*/

//Get jurisdiction
router.post('/juris/find', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB
  console.log(req.body);
  DivJuris.find({$text: {$search: req.body.pincode}})
       .exec(function(error, docs) {
		   if (error) return console.log(error);
			if(docs.length == 1) {
				console.log(docs[0]);
				res.status(200).send(docs[0]);
				return;
			}
			var alph = req.body.alph;
			for (var i = 0;i<docs.length;i++){
				divJuris=docs[i];
				if(alph >= divJuris.startAlph && alph<= divJuris.endAlph){  
					console.log(divJuris);
					res.status(200).send(divJuris);
					return;	
				}
			}
			res.status(200).json({});
	   });
});

/*
router.post('/juris/div/update', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB
  console.log(req.body);
  var divJuris = new DivJuris(req.body);
  var query = {div:req.body.div},
    options = { upsert: true, new: true, setDefaultsOnInsert: true };

// Find the document
DivJuris.findOneAndUpdate(query, req.body, options, function(error, result) {
    if (error) return console.log(error);
	console.log("result of update.");
	console.log(result);
	if (!result) {
		console.log("Doc not found. So saving it");
		console.log(divJuris);
        divJuris.save(function (err) {
			if (err) return console.error(err);
		});
    }
    // do something with the document
});
//  divJuris.save(function (err) {
//  if (err) return console.error(err);
//  });
  res.status(200).send();
});

router.get('/juris/div/all', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB
  console.log(req.body);
  DivJuris.find({}, {'_id': 0,'div':1}, function(err, divs){
      if(err) return console.log(err);
	  console.log(divs);
      res.status(200).send(divs);
  });

});

router.post('/juris/div/fetch', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB
  console.log(req.body);
  DivJuris.findOne(req.body, {'_id': 0, '__v':0}, function(err, divJuris) {
  if (err) return console.log(err);

  // object of the user
  console.log(divJuris);
  res.status(200).send(divJuris);
  
  });
});

router.post('/juris/div/delete', (req, res) => {
  // Get posts from the mock api
  // This should ideally be replaced with a service that connects to MongoDB
  console.log(req.body);
  DivJuris.deleteOne(req.body, function(err, result) {
  if (err) return console.log(err);

  // object of the user
  console.log('Deleted division ' + req.body.div);  
  });
  res.status(200).send();
});
*/

module.exports = router;