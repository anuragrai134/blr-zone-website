var mongoose = require("mongoose");

var divJurisSchema = new mongoose.Schema({
	div:{
		type: String,
		index: true
	},
    comm: String,
    pincodes: String,
	divAddr: String,
	divPhone: String,
	divEmail: String,
	commAddr: String,
	startAlph: String,
	endAlph: String,
	refTN: String
});
divJurisSchema.index({pincodes: 'text'});
var DivJuris = mongoose.model('div_juris', divJurisSchema);

module.exports = {
  DivJuris: DivJuris
}