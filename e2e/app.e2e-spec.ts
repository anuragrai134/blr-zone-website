import { JfFrontendPage } from './app.po';

describe('jf-frontend App', () => {
  let page: JfFrontendPage;

  beforeEach(() => {
    page = new JfFrontendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
