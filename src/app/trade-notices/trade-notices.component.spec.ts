import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeNoticesComponent } from './trade-notices.component';

describe('TradeNoticesComponent', () => {
  let component: TradeNoticesComponent;
  let fixture: ComponentFixture<TradeNoticesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradeNoticesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeNoticesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
