import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcNotifsComponent } from './cc-notifs.component';

describe('CcNotifsComponent', () => {
  let component: CcNotifsComponent;
  let fixture: ComponentFixture<CcNotifsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcNotifsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcNotifsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
