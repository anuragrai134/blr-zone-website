import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CtrNotifsComponent } from './ctr-notifs.component';

describe('CtrNotifsComponent', () => {
  let component: CtrNotifsComponent;
  let fixture: ComponentFixture<CtrNotifsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CtrNotifsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CtrNotifsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
