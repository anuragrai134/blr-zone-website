import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItrNotifsComponent } from './itr-notifs.component';

describe('ItrNotifsComponent', () => {
  let component: ItrNotifsComponent;
  let fixture: ComponentFixture<ItrNotifsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItrNotifsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItrNotifsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
