import { TestBed, inject } from '@angular/core/testing';

import { JurisService } from './juris.service';

describe('JurisService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JurisService]
    });
  });

  it('should be created', inject([JurisService], (service: JurisService) => {
    expect(service).toBeTruthy();
  }));
});
