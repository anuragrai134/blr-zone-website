import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UtrNotifsComponent } from './utr-notifs.component';

describe('UtrNotifsComponent', () => {
  let component: UtrNotifsComponent;
  let fixture: ComponentFixture<UtrNotifsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtrNotifsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UtrNotifsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
