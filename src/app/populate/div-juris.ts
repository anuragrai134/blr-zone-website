import { Range }    from './range';

export class DivJuris {

  constructor(
	public div: string,
    public comm: string,
    public pincodes: string,
	public divAddr: string,
	public divPhone: string,
	public divEmail: string,
	public commAddr: string,
	public startAlph: string,
	public endAlph: string,
	public refTN: string
  ) {  }

}