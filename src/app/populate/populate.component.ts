import { Component, OnInit } from '@angular/core';
import { DivJuris } from './div-juris';
import { JurisService} from '../juris.service';

@Component({
  selector: 'app-populate',
  templateUrl: './populate.component.html',
  styleUrls: ['./populate.component.css']
})
export class PopulateComponent implements OnInit {

  constructor(private jurisService: JurisService) { }

  ngOnInit() {
  }
 
  existingDivs : DivJuris[] = [];
  model = new DivJuris('','','','','','','','','','');
  
  submitted = false;
  hideForm=true;
  edit=false;
  
 
  onSubmit() { 
	this.submitted = true;
	console.log(this.model);
	this.jurisService.updateDiv(this.model).subscribe(res => {
      console.log(res);
    });
  }
  
  addDiv() {
	this.hideForm=false;  
	this.edit=false;
  }
  
  editDiv() {
	this.hideForm=false;
	this.edit=true;
	this.jurisService.getAllDivs().subscribe(res => {
      console.log(res);
	  this.existingDivs = res;
	  console.log(this.existingDivs);
    });
  }
  
  deleteDiv() {
	this.jurisService.deleteDiv(this.model.div).subscribe(res => {
 	  console.log(res);
    });
	this.model = new DivJuris('','','','','','','','','','');
	//model = new DivJuris();  
	this.existingDivs = [];
	this.hideForm=true;  
  }
  
  divSelected(div : String) {
	this.jurisService.getDiv(div).subscribe(res => {
      this.model = res;
	  console.log(this.model);
    });  
  }
  
  
  
}
