import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcrNotifsComponent } from './ccr-notifs.component';

describe('CcrNotifsComponent', () => {
  let component: CcrNotifsComponent;
  let fixture: ComponentFixture<CcrNotifsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcrNotifsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcrNotifsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
