export class JurisResponse {

  constructor(
    public comm: string,
    public div: string,
	public range: string,
	public commAddr: string,
	public divAddr: string,
	public rangeAddr: string
  ) {  }

}