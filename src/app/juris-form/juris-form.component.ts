import { Component, OnInit } from '@angular/core';
import { JurisRequest }    from './juris-request';
//import { JurisResponse }    from './juris-response';
import { JurisService} from '../juris.service';
import { DivJuris } from '../populate/div-juris';


@Component({
  selector: 'juris-form',
  templateUrl: './juris-form.component.html',
  styleUrls: ['./juris-form.component.css']
})
export class JurisFormComponent implements OnInit {

  constructor(private jurisService: JurisService) { }

  ngOnInit() {
  }
  
  alphabets = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
  
  model = new JurisRequest('560043', this.alphabets[0]);
  juris : DivJuris;
 
  submitted = false;
 
  onSubmit() { 
	this.submitted = true;
	console.log(this.model);
	this.jurisService.getJuris(this.model).subscribe(res => {
      this.juris = res;
    });
	console.log(this.juris);
  }
 
}
