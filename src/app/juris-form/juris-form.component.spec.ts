import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JurisFormComponent } from './juris-form.component';

describe('JurisFormComponent', () => {
  let component: JurisFormComponent;
  let fixture: ComponentFixture<JurisFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JurisFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JurisFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
