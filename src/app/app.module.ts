import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { PostsComponent } from './posts/posts.component';

import { PostsService } from './posts.service';
import { JurisService} from './juris.service';
import { JurisFormComponent } from './juris-form/juris-form.component';
import { PopulateComponent } from './populate/populate.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { PccMsgComponent } from './pcc-msg/pcc-msg.component';
import { CircularsComponent } from './circulars/circulars.component';
import { TradeNoticesComponent } from './trade-notices/trade-notices.component';
import { DownloadsComponent } from './downloads/downloads.component';
import { UsefulLinksComponent } from './useful-links/useful-links.component';
import { MediaGalleryComponent } from './media-gallery/media-gallery.component';
import { WhatsNewComponent } from './whats-new/whats-new.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { RtiComponent } from './rti/rti.component';
import { VisitUsComponent } from './visit-us/visit-us.component';
import { HomeComponent } from './home/home.component';
import { CtNotifsComponent } from './ct-notifs/ct-notifs.component';
import { CtrNotifsComponent } from './ctr-notifs/ctr-notifs.component';
import { ItNotifsComponent } from './it-notifs/it-notifs.component';
import { ItrNotifsComponent } from './itr-notifs/itr-notifs.component';
import { UtNotifsComponent } from './ut-notifs/ut-notifs.component';
import { UtrNotifsComponent } from './utr-notifs/utr-notifs.component';
import { CcNotifsComponent } from './cc-notifs/cc-notifs.component';
import { CcrNotifsComponent } from './ccr-notifs/ccr-notifs.component';
import { CusNotifsComponent } from './cus-notifs/cus-notifs.component';
import { FaqsComponent } from './faqs/faqs.component';

// Define the routes
const ROUTES = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  { path: 'find', component: JurisFormComponent},
  { path: 'populate',component: PopulateComponent},
  { path: 'home',component: HomeComponent},
  { path: 'home/about-us',component: AboutUsComponent},
  { path: 'home/pcc-msg',component: PccMsgComponent},
  { path: 'rti',component: RtiComponent},
  { path: 'notifs/ct', component: CtNotifsComponent},
  { path: 'notifs/ctr', component: CtrNotifsComponent},
  { path: 'notifs/it', component: ItNotifsComponent},
  { path: 'notifs/itr', component: ItrNotifsComponent},
  { path: 'notifs/ut', component: UtNotifsComponent},
  { path: 'notifs/utr', component: UtrNotifsComponent},
  { path: 'notifs/cc', component: CcNotifsComponent},
  { path: 'notifs/ccr', component: CcrNotifsComponent},
  { path: 'notifs/cus', component: CusNotifsComponent},
  { path: 'circulars',component: CircularsComponent},
  { path: 'trade-notices',component: TradeNoticesComponent},
  { path: 'downloads',component: DownloadsComponent},
  { path: 'faqs',component: FaqsComponent},
  { path: 'useful-links',component: UsefulLinksComponent},
  { path: 'media-gallery',component: MediaGalleryComponent},
  { path: 'whats-new',component: WhatsNewComponent},
  { path: 'contact-us',component: ContactUsComponent},
  { path: 'visit-us',component: VisitUsComponent}
  
];

@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    JurisFormComponent,
    PopulateComponent,
    AboutUsComponent,
    PccMsgComponent,
    CircularsComponent,
    TradeNoticesComponent,
    DownloadsComponent,
    UsefulLinksComponent,
    MediaGalleryComponent,
    WhatsNewComponent,
    ContactUsComponent,
    RtiComponent,
    VisitUsComponent,
    HomeComponent,
    CtNotifsComponent,
    CtrNotifsComponent,
    ItNotifsComponent,
    ItrNotifsComponent,
    UtNotifsComponent,
    UtrNotifsComponent,
    CcNotifsComponent,
    CcrNotifsComponent,
    CusNotifsComponent,
    FaqsComponent
  ],
  imports: [
    BrowserModule,
	FormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES), // Add routes to the app
	NgbModule.forRoot()
  ],
  providers: [PostsService, JurisService],
  bootstrap: [AppComponent]
})
export class AppModule { }
