import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PccMsgComponent } from './pcc-msg.component';

describe('PccMsgComponent', () => {
  let component: PccMsgComponent;
  let fixture: ComponentFixture<PccMsgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PccMsgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PccMsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
