import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CusNotifsComponent } from './cus-notifs.component';

describe('CusNotifsComponent', () => {
  let component: CusNotifsComponent;
  let fixture: ComponentFixture<CusNotifsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CusNotifsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CusNotifsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
