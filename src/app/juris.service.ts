import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { JurisRequest }    from './juris-form/juris-request';
import { DivJuris } from './populate/div-juris';

@Injectable()
export class JurisService {

  constructor(private http: Http) { }

  // Get all posts from the API
  getJuris(jurisRequest: JurisRequest) {
    return this.http.post('/api/juris/find', jurisRequest)
      .map(res => res.json());
  }
  
  updateDiv(divJuris: DivJuris) {
	return this.http.post('/api/juris/div/update', divJuris);
  }

  deleteDiv(div: String) {
	return this.http.post('/api/juris/div/delete', {'div': div});  
  }
  getAllDivs(){
	return this.http.get('/api/juris/div/all')
	  .map(res => res.json());  
  }
  
  getDiv(div: String) {
	return this.http.post('/api/juris/div/fetch', {'div': div})
	  .map(res => res.json());    
  }
}
