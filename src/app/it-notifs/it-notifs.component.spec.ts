import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItNotifsComponent } from './it-notifs.component';

describe('ItNotifsComponent', () => {
  let component: ItNotifsComponent;
  let fixture: ComponentFixture<ItNotifsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItNotifsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItNotifsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
