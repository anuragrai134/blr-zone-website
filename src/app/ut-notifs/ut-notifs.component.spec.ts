import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UtNotifsComponent } from './ut-notifs.component';

describe('UtNotifsComponent', () => {
  let component: UtNotifsComponent;
  let fixture: ComponentFixture<UtNotifsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtNotifsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UtNotifsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
