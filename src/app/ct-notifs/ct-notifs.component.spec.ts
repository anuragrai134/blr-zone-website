import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CtNotifsComponent } from './ct-notifs.component';

describe('CtNotifsComponent', () => {
  let component: CtNotifsComponent;
  let fixture: ComponentFixture<CtNotifsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CtNotifsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CtNotifsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
